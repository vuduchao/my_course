$(document).ready(function () {
    $('.h-select-time').click(function () {
        $('.h-bubble-time').toggle();
        $('.h-bubble-name').hide();
    });
    $('.h-select-name').click(function () {
        $('.h-bubble-name').toggle();
        $('.h-bubble-time').hide();
    });
    $('.h-item-filter').click(function(){
      $('.h-item-filter.active').removeClass('active');
      $(this).addClass('active');
    });
    $('.h-itemcustom').click(function(){
        $('.h-select-day').css('display','inline-block');
    })
    //slide recommend
    $('#lichsu_recomend').owlCarousel({
        loop:true,
        margin:20,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    })
});