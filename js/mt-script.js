$(document).ready(function(){
    $('[data-click="video"]').click(function(){
        $('[data-click="video"]').removeClass('active');
        $(this).addClass('active');
        return false;
    });

    $(document).on('click', '[data-click="more"]', function (e) {
        if ($(this).hasClass('active')) {
        	console.log(11);
        	$(this).parents('.content-tab').find('.mt-content-tabs').removeAttr('style');
        	$(this).text('Xem thêm').removeClass('active');
        }else{
        	$(this).parents('.content-tab').find('.mt-content-tabs').css('max-height', 'none');
        	$(this).text('Thu gọn').addClass('active');
        }
        return false;
    });

    $(document).on('click', '[data-light="click"]', function (e) {
    	if ($(this).hasClass('active')) {
    		var text = $(this).text();
    		if (text) {
    			$(this).find('span').text('Tắt')
    		}
    		$(this).removeClass('active');
    		$('body').removeClass('dark-page');
    	}else{
    		var text = $(this).text();
    		if (text) {
    			$(this).find('span').text('Bật')
    		}
    		$(this).addClass('active');
    		$('body').addClass('dark-page');
    	}

    	return false;
    });

    $(document).on('click', '.moblie-menu-btn, .offcanvas-overflow, .box-logo .back_page', function (e) {
    	$('body').toggleClass('uk-offcanvas-page')
        if (!$('#offcanvas').hasClass('show')) {
            $('#offcanvas').addClass('show');
        }
    	return false;
    });

    $(document).on('click', '.mt-process-video', function (e) {
    	if ($(this).hasClass('active')) {
    		$(this).removeClass('active');
    	}else{
    		$(this).addClass('active');
    	}
    	return false;
    });

    $('#thread-owlcarosel').owlCarousel({
        loop: true,
        margin: 30,
        nav: true,
        dots: false,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:4
            }
        }
    })
});